<?php

require_once('Frog.php');
require_once('Ape.php');

$sheep = new Animal("shaun");

echo "Name : $sheep->name <br>"; // "shaun"
echo "Legs : $sheep->legs <br>"; // 4
echo "cold blooded : $sheep->cold_blooded<br><br>"; // "no"
 
$buduk = new Frog("Buduk");

echo "Name : $buduk->name<br>";
echo "Legs : $buduk->legs <br>";
echo "cold blooded : $buduk->cold_blooded <br>";
echo "Jump : $buduk->jump <br><br>";

$sungokong = new Ape("Kera Sakti");
echo "Name : $sungokong->name<br>";
echo "Legs : $sungokong->legs2 <br>";
echo "cold blooded : $sungokong->cold_blooded <br>";
echo "Yell : $sungokong->yell<br><br>";

 